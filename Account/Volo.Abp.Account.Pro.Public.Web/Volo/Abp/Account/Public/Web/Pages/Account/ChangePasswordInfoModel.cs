﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;
using Volo.Abp.Identity;
using Volo.Abp.Validation;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
    public class ChangePasswordInfoModel
	{
		[DataType(DataType.Password)]
		[DisableAuditing]
		[Display(Name = "DisplayName:CurrentPassword")]
		[Required]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxPasswordLength", null)]
		public string CurrentPassword { get; set; }

		[DataType(DataType.Password)]
		[DisableAuditing]
		[Required]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxPasswordLength", null)]
		[Display(Name = "DisplayName:NewPassword")]
		public string NewPassword { get; set; }

		[DynamicStringLength(typeof(IdentityUserConsts), "MaxPasswordLength", null)]
		[DisableAuditing]
		[Display(Name = "DisplayName:NewPasswordConfirm")]
		[Required]
		[DataType(DataType.Password)]
		public string NewPasswordConfirm { get; set; }
	}
}
