﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class PasswordResetLinkSentModel : AccountPageModel
	{
		public virtual Task<IActionResult> OnGetAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}

		public virtual Task<IActionResult> OnPostAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}
	}
}
