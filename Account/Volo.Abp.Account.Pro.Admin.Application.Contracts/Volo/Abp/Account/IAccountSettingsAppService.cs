﻿using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Volo.Abp.Account
{
    public interface IAccountSettingsAppService : IRemoteService, IApplicationService
	{
		Task<AccountSettingsDto> GetAsync();

		Task UpdateAsync(AccountSettingsDto input);
	}
}
