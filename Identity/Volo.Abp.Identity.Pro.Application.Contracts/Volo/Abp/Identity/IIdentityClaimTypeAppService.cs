﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Volo.Abp.Identity
{
	public interface IIdentityClaimTypeAppService : IApplicationService, IRemoteService
	{
		Task<PagedResultDto<ClaimTypeDto>> GetListAsync(GetIdentityClaimTypesInput input);

		Task<List<ClaimTypeDto>> GetAllListAsync();

		Task<ClaimTypeDto> GetAsync(Guid id);

		Task<ClaimTypeDto> CreateAsync(CreateClaimTypeDto input);

		Task<ClaimTypeDto> UpdateAsync(Guid id, UpdateClaimTypeDto input);

		Task DeleteAsync(Guid id);
	}
}
