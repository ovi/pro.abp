﻿using Volo.Abp.Application.Dtos;

namespace Volo.Abp.Identity
{
	public class GetIdentityRoleListInput : PagedAndSortedResultRequestDto
	{
		public string Filter { get; set; }
	}
}
