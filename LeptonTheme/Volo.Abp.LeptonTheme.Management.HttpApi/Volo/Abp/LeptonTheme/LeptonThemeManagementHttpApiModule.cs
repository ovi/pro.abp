﻿using Localization.Resources.AbpUi;
using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.LeptonTheme.Management.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;

namespace Volo.Abp.LeptonTheme
{
    [DependsOn(
		typeof(LeptonThemeManagementApplicationContractsModule),
		typeof(AbpAspNetCoreMvcModule)
	)]
	public class LeptonThemeManagementHttpApiModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			base.PreConfigure<IMvcBuilder>( mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(LeptonThemeManagementHttpApiModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpLocalizationOptions>(options =>
			{
				options.Resources.Get<LeptonThemeManagementResource>().AddBaseTypes(new Type[]
				{
					typeof(AbpUiResource)
				});
			});
		}
	}
}
