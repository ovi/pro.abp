﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.JsTree;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Commercial;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.ObjectMapping;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Libraries.JsTree;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Toolbars;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;
using Volo.Abp.AspNetCore.Mvc.UI.Theming;
using Volo.Abp.AutoMapper;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton
{
    [DependsOn(
		typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
		typeof(LeptonThemeManagementWebModule),
		typeof(AbpAutoMapperModule),
		typeof(AbpAspNetCoreMvcUiThemeCommercialModule)
	)]
	public class AbpAspNetCoreMvcUiLeptonThemeModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpAspNetCoreMvcUiLeptonThemeModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpThemingOptions>(options =>
			{
				options.Themes.Add<LeptonTheme>();
				if (options.DefaultThemeName == null)
				{
					options.DefaultThemeName = "Lepton";
				}
			});
			base.Configure<AbpErrorPageOptions>(options =>
			{
				options.ErrorViewUrls.Add("401", "~/Views/Error/401.cshtml");
				options.ErrorViewUrls.Add("403", "~/Views/Error/403.cshtml");
				options.ErrorViewUrls.Add("404", "~/Views/Error/404.cshtml");
				options.ErrorViewUrls.Add("500", "~/Views/Error/500.cshtml");
			});
			base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<AbpAspNetCoreMvcUiLeptonThemeModule>();
			});
			base.Configure<AbpToolbarOptions>(options =>
			{
				options.Contributors.Add(new LeptonThemeMainTopToolbarContributor());
			});
			base.Configure<AbpBundlingOptions>(options =>
			{
				options.StyleBundles.Add("Lepton.Global", bundle =>
				{
					BundleConfigurationExtensions.AddContributors(BundleConfigurationExtensions.AddBaseBundles(bundle, new string[]
					{
						StandardBundles.Styles.Global
					}), new Type[]
					{
						typeof(LeptonGlobalStyleContributor)
					});
				});
				options.ScriptBundles.Add("Lepton.Global", bundle =>
				{
					BundleConfigurationExtensions.AddContributors(BundleConfigurationExtensions.AddBaseBundles(bundle, new string[]
					{
						StandardBundles.Scripts.Global
					}), new Type[]
					{
						typeof(LeptonGlobalScriptContributor)
					});
				});
			});
			base.Configure<AbpBundleContributorOptions>(options =>
			{
				options.Extensions<JsTreeStyleContributor>().Add<LeptonJsTreeStyleContributorExtension>();
			});
			context.Services.AddAutoMapperObjectMapper<AbpAspNetCoreMvcUiLeptonThemeModule>();
			base.Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddProfile<LeptonThemeAutoMapperProfile>(true);
			});
		}
	}
}
