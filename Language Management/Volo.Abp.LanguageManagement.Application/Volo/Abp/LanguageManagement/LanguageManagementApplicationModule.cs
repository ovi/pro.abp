﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.AutoMapper;
using Volo.Abp.LanguageManagement.Dto;
using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending.Modularity;
using Volo.Abp.SettingManagement;

namespace Volo.Abp.LanguageManagement
{
	[DependsOn(
		typeof(LanguageManagementDomainModule),
		typeof(LanguageManagementApplicationContractsModule),
		typeof(AbpAutoMapperModule),
		typeof(AbpSettingManagementDomainModule)
	)]
	public class LanguageManagementApplicationModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			ModuleExtensionConfigurationHelper.ApplyEntityConfigurationToApi("LanguageManagement", "Language", new Type[]
			{
				typeof(LanguageDto)
			}, new Type[]
			{
				typeof(CreateLanguageDto)
			}, new Type[]
			{
				typeof(UpdateLanguageDto)
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			context.Services.AddAutoMapperObjectMapper<LanguageManagementApplicationModule>();
			base.Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddProfile<LanguageManagementApplicationAutoMapperProfile>(true);
			});
		}
	}
}
