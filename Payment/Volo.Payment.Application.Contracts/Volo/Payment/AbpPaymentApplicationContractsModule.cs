﻿using Volo.Abp.Application;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using Volo.Payment.Localization;

namespace Volo.Payment
{
	[DependsOn(
		typeof(AbpPaymentDomainSharedModule),
		typeof(AbpDddApplicationModule)
	)]
	public class AbpPaymentApplicationContractsModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options => options.FileSets.AddEmbedded<AbpPaymentApplicationContractsModule>());
			base.Configure<AbpLocalizationOptions>(options => options.Resources.Get<PaymentResource>().AddVirtualJson("/Volo/Payment/Localization/ApplicationContracts"));
		}
	}
}
